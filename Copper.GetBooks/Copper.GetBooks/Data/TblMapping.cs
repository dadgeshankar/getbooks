﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Copper.GetBooks.Data
{
    public class TblMapping
    {
        public int Id { get; set; }
        public int BranchId { get; set; }
        public int BookId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }

        public TblBook Book { get; set; }
        public TblBranch Branch { get; set; }
    }
}
