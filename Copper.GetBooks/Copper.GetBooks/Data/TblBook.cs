﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Copper.GetBooks.Data
{
    public class TblBook
    {
        public TblBook()
        {
            TblMapping = new HashSet<TblMapping>();
        }

        public int BookId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public string Pdfurl { get; set; }
        public string Author { get; set; }
        public string Edition { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public int? SubCategoryId { get; set; }
        public TblSubCategory SubCategory { get; set; }
        public ICollection<TblMapping> TblMapping { get; set; }
    }
}
