﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Copper.GetBooks.Data
{
    public partial class TblBranch
    {
        public TblBranch()
        {
            TblMapping = new HashSet<TblMapping>();
        }

        public int BranchId { get; set; }
        public int ClassId { get; set; }
        public string BranchName { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }

        public TblClass Class { get; set; }
        public ICollection<TblMapping> TblMapping { get; set; }
    }
}
