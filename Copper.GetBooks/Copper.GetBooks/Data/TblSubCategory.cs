﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Copper.GetBooks.Data
{
    public class TblSubCategory
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CategoryId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public ICollection<TblBook> TblBook { get; set; }
        public TblCategory Category { get; set; }
    }
}
