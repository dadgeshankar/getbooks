﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Copper.GetBooks.Data
{
    public class TblStudent
    {
        public Int16 Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string MobileNo { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public Int16 ClassId { get; set; }
        public Int16 BranchId { get; set; }
        public string Year { get; set; }
        public bool IsApproved { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
