﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Copper.GetBooks.Data
{
    public class DataContext : DbContext
    {
        readonly string sqlConnectionString = Environment.GetEnvironmentVariable("DefaultConnection");
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySql(sqlConnectionString);
        }
        public virtual DbSet<TblBook> TblBook { get; set; }
        public virtual DbSet<TblBranch> TblBranch { get; set; }
        public virtual DbSet<TblClass> TblClass { get; set; }
        public virtual DbSet<TblMapping> TblMapping { get; set; }
        public virtual DbSet<TblStudent> TblStudent { get; set; }
        public virtual DbSet<TblCategory> TblCategory { get; set; }
        public virtual DbSet<TblSubCategory> TblSubCategory { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<TblBook>(entity =>
            {
                entity.HasKey(e => e.BookId);

                entity.HasIndex(e => e.SubCategoryId)
                    .HasName("fk_subcat_bksubcat_idx");

                entity.Property(e => e.BookId).HasColumnType("int(11)");

                entity.Property(e => e.Author).HasColumnType("varchar(45)");

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasColumnType("varchar(45)");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.Description).HasColumnType("varchar(500)");

                entity.Property(e => e.Edition).HasColumnType("varchar(45)");

                entity.Property(e => e.ImageUrl).HasColumnType("varchar(500)");

                entity.Property(e => e.ModifiedBy)
                    .IsRequired()
                    .HasColumnType("varchar(45)");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Pdfurl)
                    .IsRequired()
                    .HasColumnName("PDFUrl")
                    .HasColumnType("varchar(500)");

                entity.Property(e => e.SubCategoryId).HasColumnType("int(11)");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasColumnType("varchar(45)");

                entity.HasOne(d => d.SubCategory)
                    .WithMany(p => p.TblBook)
                    .HasForeignKey(d => d.SubCategoryId)
                    .HasConstraintName("fk_subcat_bksubcat");
            });

            modelBuilder.Entity<TblBranch>(entity =>
            {
                entity.HasKey(e => e.BranchId);

                entity.HasIndex(e => e.ClassId)
                    .HasName("fk_ClsId_BrClsId_idx");

                entity.Property(e => e.BranchId).HasColumnType("int(11)");

                entity.Property(e => e.BranchName)
                    .IsRequired()
                    .HasColumnType("varchar(45)");

                entity.Property(e => e.ClassId).HasColumnType("int(11)");

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasColumnType("varchar(45)");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.ModifiedBy)
                    .IsRequired()
                    .HasColumnType("varchar(45)");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Class)
                    .WithMany(p => p.TblBranch)
                    .HasForeignKey(d => d.ClassId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_ClsId_BrClsId");
            });
            modelBuilder.Entity<TblCategory>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasColumnType("varchar(45)");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.ModifiedBy)
                    .IsRequired()
                    .HasColumnType("varchar(45)");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnType("varchar(45)");
            });
            modelBuilder.Entity<TblClass>(entity =>
            {
                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasColumnType("varchar(45)");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.ModifiedBy)
                    .IsRequired()
                    .HasColumnType("varchar(45)");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnType("varchar(45)");
            });

            modelBuilder.Entity<TblMapping>(entity =>
            {
                entity.HasIndex(e => e.BookId)
                    .HasName("fk_mapBkId_BkId_idx");

                entity.HasIndex(e => e.BranchId)
                    .HasName("fk_mapBrId_BrId_idx");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.BookId).IsRequired().HasColumnType("int(11)");

                entity.Property(e => e.BranchId).IsRequired().HasColumnType("int(11)");

                entity.Property(e => e.CreatedBy).HasColumnType("varchar(45)");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.ModifiedBy).HasColumnType("varchar(45)");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.HasOne(d => d.Book)
                    .WithMany(p => p.TblMapping)
                    .HasForeignKey(d => d.BookId)
                    .HasConstraintName("fk_mapBkId_BkId");

                entity.HasOne(d => d.Branch)
                    .WithMany(p => p.TblMapping)
                    .HasForeignKey(d => d.BranchId)
                    .HasConstraintName("fk_mapBrId_BrId");
            });

            modelBuilder.Entity<TblStudent>(entity => {
                entity.HasKey(m => m.Id);
                entity.Property(e => e.Name).HasColumnType("VARCHAR(16)");
                entity.Property(e => e.Email).HasColumnType("VARCHAR(50)");
                entity.Property(e => e.MobileNo).HasColumnType("VARCHAR(15)");
                entity.Property(e => e.Password).HasColumnType("VARCHAR(15)");
                //entity.Property(e => e.ConfirmPassword).HasColumnType("VARCHAR(15)");
                entity.Property(e => e.ClassId).HasColumnType("int(11)");
                entity.Property(e => e.BranchId).HasColumnType("int(11)");
                entity.Property(e => e.Year).HasColumnType("VARCHAR(50)");
                entity.Property(e => e.IsApproved).HasColumnType("bool");
                entity.Property(e => e.CreatedOn).IsRequired().HasColumnType("datetime");
                entity.Property(e => e.ModifiedOn).IsRequired().HasColumnType("datetime");

            });
            modelBuilder.Entity<TblSubCategory>(entity =>
            {
                entity.HasIndex(e => e.CategoryId)
                    .HasName("fk_cat_subcat_idx");

                entity.Property(e => e.Id).HasColumnType("int(11)");

                entity.Property(e => e.CategoryId).HasColumnType("int(11)");

                entity.Property(e => e.CreatedBy).HasColumnType("varchar(45)");

                entity.Property(e => e.CreatedOn).HasColumnType("datetime");

                entity.Property(e => e.ModifiedBy).HasColumnType("varchar(45)");

                entity.Property(e => e.ModifiedOn).HasColumnType("datetime");

                entity.Property(e => e.Name).HasColumnType("varchar(45)");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.TblSubCategory)
                    .HasForeignKey(d => d.CategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("fk_cat_subcat");
            });
        }

    }
}
