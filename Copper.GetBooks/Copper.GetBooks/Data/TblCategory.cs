﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Copper.GetBooks.Data
{
    public class TblCategory
    {
        public TblCategory()
        {
            TblSubCategory = new HashSet<TblSubCategory>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }

        public ICollection<TblSubCategory> TblSubCategory { get; set; }
    }
}
