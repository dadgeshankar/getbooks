﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Copper.GetBooks
{
    public class ApiResponse
    {
        public ApiResponse()
        {
            Data = new List<Book>();
        }
        public int StatusCode { get; set; }
        public string Message { get; set; }
        public List<Book> Data { get; set; }
    }
    public class Book
    {
        public int BookId { get; set; }
        public string Title { get; set; }
        public string ImageUrl { get; set; }
        public string Pdfurl { get; set; }
        public string Author { get; set; }
        public string Edition { get; set; }
        public string Category { get; set; }
        public string SubCategory { get; set; }
    }
}
