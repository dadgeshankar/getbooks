using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Amazon.Lambda.Core;
using Copper.GetBooks.Data;
using System.Net;
using Amazon.Lambda.APIGatewayEvents;
// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace Copper.GetBooks
{
    public class Function
    {

        /// <summary>
        /// A simple function that takes a string and does a ToUpper
        /// </summary>
        /// <param name="input"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public ApiResponse FunctionHandler(APIGatewayProxyRequest request, ILambdaContext context)
        {
            int id=Convert.ToInt32( request.QueryStringParameters["id"]);
            ApiResponse apiResponse = new ApiResponse();
            try
            {
                using (DataContext dataContext = new DataContext())
                {
                    var student = dataContext.TblStudent.FirstOrDefault(x => x.Id == id);
                    if (student == null || student.IsApproved == false)
                    {
                        apiResponse.StatusCode = (int)HttpStatusCode.Unauthorized;
                        apiResponse.Message = "Student approval is pending, Please contact to Admin";
                        return apiResponse;
                    }

                    var list = (from bk in dataContext.TblBook
                                join
                                cat in dataContext.TblSubCategory on bk.SubCategoryId equals cat.Id
                                join
                                scat in dataContext.TblCategory on cat.CategoryId equals scat.Id
                                join
                                mp in dataContext.TblMapping on bk.BookId equals mp.BookId
                                join
                                br in dataContext.TblBranch on mp.BranchId equals br.BranchId
                                select new { bk, br,cat,scat}).Where(x => x.br.ClassId == id).ToList();

                    if (list == null || list.Count == 0)
                    {
                        apiResponse.StatusCode = (int)HttpStatusCode.NoContent;
                        apiResponse.Message = "No data found";
                        return apiResponse;
                    }
                    foreach (var item in list.Distinct())
                    {
                        if (item.bk != null)
                        {
                            var book = new Book();
                            book.BookId = item.bk.BookId;
                            book.Title = item.bk.Title;
                            book.Author = item.bk.Author;
                            book.Edition = item.bk.Edition;
                            book.ImageUrl = item.bk.ImageUrl;
                            book.Pdfurl = item.bk.Pdfurl;
                            book.SubCategory = item.scat != null ? item.scat.Name : string.Empty;
                            book.Category = item.cat != null  ? item.cat.Name : string.Empty;
                            apiResponse.Data.Add(book);
                        }
                    }

                    apiResponse.Data = apiResponse.Data.ToList();
                    apiResponse.StatusCode = (int)HttpStatusCode.OK;
                    apiResponse.Message = "Data featched successfully";

                }
            }
            catch(Exception ex)
            {
                apiResponse.StatusCode = (int)HttpStatusCode.InternalServerError;
                apiResponse.Message = "Error occored while getting books";
            }
            return apiResponse;
        }
    }
}
