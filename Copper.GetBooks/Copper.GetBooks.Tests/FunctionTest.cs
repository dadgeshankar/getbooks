using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Xunit;
using Amazon.Lambda.Core;
using Amazon.Lambda.TestUtilities;

using Copper.GetBooks;
using Amazon.Lambda.APIGatewayEvents;

namespace Copper.GetBooks.Tests
{
    public class FunctionTest
    {
       public FunctionTest()
        {

            Environment.SetEnvironmentVariable("DefaultConnection", "Server=mysqlserver.cjgofnupcr7h.ap-south-1.rds.amazonaws.com; userid=root;password=ayavamas;database=Copper;");
        }
        [Fact]
        public void TestToUpperFunction()
        {
            APIGatewayProxyRequest aPIGatewayProxyRequest = new APIGatewayProxyRequest();
            Dictionary<string, string> dictionary = new Dictionary<string, string>();
            dictionary.Add("id", "1");
            aPIGatewayProxyRequest.QueryStringParameters = dictionary;
            // Invoke the lambda function and confirm the string was upper cased.
            var function = new Function();
            var context = new TestLambdaContext();
            var apiResponse = function.FunctionHandler(aPIGatewayProxyRequest, context);
            Assert.Equal(200, apiResponse.StatusCode);
            Assert.True(apiResponse.Data.Count >0);
        }
    }
}
